package com.example.motivation.problem.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author Klaudia Brisakova
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("find/{userId}")
    public User getUserById(@PathVariable Long userId) {
        return userService.findUserById(userId);
    }

    @GetMapping("find/all")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping("create")
    public User createUser(@Validated(User.OnCreate.class) @RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("edit/{userId}")
    public User updateUser(@Validated(User.OnUpdate.class) @RequestBody User updated,
                           @PathVariable Long userId) {
        return userService.updateNameOfUser(userId, updated);
    }
}
