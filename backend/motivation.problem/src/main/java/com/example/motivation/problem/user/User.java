package com.example.motivation.problem.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

/**
 * @author Klaudia Brisakova
 */
@Entity
@Table(name = "users")
public class User {

    interface OnCreate {}

    interface OnUpdate {}

    @Id
    @GeneratedValue
    @Null(groups = OnCreate.class)
    @NotNull(groups = OnUpdate.class)
    private Long id;

    @NotNull(groups = {OnCreate.class, OnUpdate.class})
    @Size(min=2, max=20, groups = {OnUpdate.class, OnCreate.class})
    private String name;

    @NotNull(groups = OnCreate.class)
    @Null(groups = OnUpdate.class)
    @Column(unique = true)
    private String username; // deserializable only

    @NotNull(groups = OnCreate.class)
    @Null(groups = OnUpdate.class)
    @Size(min=6, max=255, groups = OnCreate.class)
    private String password; // deserializable only

    @JsonIgnore
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", getPassword='" + password + '\'' +
                '}';
    }
}