package com.example.motivation.problem.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Klaudia Brisakova
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserById(Long id);

    boolean existsUserById(Long id);

    User findUserByUsername(String username);

    boolean existsUserByUsername(String username);

}
