package com.example.motivation.problem.user;

import com.example.motivation.problem.exeption.EntityNotFoundException;

import java.util.List;

/**
 * @author Klaudia Brisakova
 */
public interface UserService {

    /**
     * Finds user by {@code id}.
     *
     * @param id id
     * @return found user
     * @throws EntityNotFoundException when user with {@code id} does not exist
     */
    User findUserById(Long id);

    /**
     * Finds all user.
     *
     * @return all user
     */
    List<User> getAllUsers();

    /**
     * Saves user to database. Password is encoded before saving.
     *
     * @param user user to save
     * @return saved user with assigned id
     */
    User createUser(User user);

    /**
     * Updates user's name with {@code userId}. User's name is updated according to parameter {@code updated}.
     *
     * @param userId id of user to update
     * @param updated update information
     * @return updated user
     * @throws EntityNotFoundException when user with userId does not exist
     */
    User updateNameOfUser(Long userId, User updated);

    /**
     * Deletes user with userId from database.
     *
     * @param userId id
     * @throws EntityNotFoundException when user with {@code userId} does not exist.
     */
    void deleteUser(Long userId);
}
