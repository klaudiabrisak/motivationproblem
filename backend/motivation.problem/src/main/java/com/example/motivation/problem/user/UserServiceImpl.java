package com.example.motivation.problem.user;

import com.example.motivation.problem.exeption.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Klaudia Brisakova
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    public final static String ENCODER_STRING = "{bcrypt}";


    @Override
    public User findUserById(Long id) {
        if (!userRepository.existsUserById(id)) {
            throw new EntityNotFoundException("User with id:" + id + " does not exist.");
        }
        return userRepository.findUserById(id);
    }

    @Override
    public User updateNameOfUser(Long userId, User updated) {
        if(!userRepository.existsUserById(userId)) {
            throw new EntityNotFoundException("User with id:" + userId + " does not exist for update.");
        }
        User toUpdate = userRepository.findUserById(userId);
        toUpdate.setName(updated.getName());

        userRepository.saveAndFlush(toUpdate);
        return toUpdate;
    }

    @Override
    public void deleteUser(Long userId) {
        if (!userRepository.existsUserById(userId)) {
            throw new EntityNotFoundException("You cannot delete user with id:" + userId + ". User does not exist.");
        }
        userRepository.deleteById(userId);
    }

    @Override
    public User createUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.saveAndFlush(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
