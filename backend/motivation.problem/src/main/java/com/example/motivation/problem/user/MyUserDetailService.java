package com.example.motivation.problem.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * @author Klaudia Brisakova
 */
@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if (!userRepository.existsUserByUsername(s)) {
            throw new UsernameNotFoundException("User with username:\"" + s + "\" not found.");
        }
        return toUserDetails(userRepository.findUserByUsername(s));
    }

    private UserDetails toUserDetails(User user) {
        return org.springframework.security.core.userdetails.User.withUsername(user.getName())
                .password(UserServiceImpl.ENCODER_STRING + user.getPassword())
                .roles("USER").build();
    }
}
