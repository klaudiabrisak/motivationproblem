import React from "react";
import Cell from './Cell';
import PropTypes from "prop-types";
import ButtonCell from "./ButtonCell";


const Row = props => (
    <tr>
        <Cell value={props.user.id}/>
        <Cell value={props.user.name}/>
        <ButtonCell value={'Delete'} onClick={() => props.onRowDeleted(props.user.id)}/>
        <ButtonCell value={'Edit'} onClick={() => props.onRowEdited(props.user)}/>
    </tr>
);

Row.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    }).isRequired,
    onRowDeleted: PropTypes.func.isRequired,
    onRowEdited: PropTypes.func.isRequired
};

export default Row;