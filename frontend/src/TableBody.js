import React from "react";
import Row from './Row'
import TableHeader from "./TableHeader";
import PropTypes from "prop-types";


const TableBody = props => (
    <tbody>
        <TableHeader/>
        {renderRows(props)}
    </tbody>
);


const renderRows = (props) => props.users.map(item => (
    <Row key={item.id} user={item} onRowDeleted={props.onRowDeleted} onRowEdited={props.onRowEdited}/>
));


TableBody.propTypes = {
    users: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    onRowDeleted: PropTypes.func.isRequired,
    onRowEdited: PropTypes.func.isRequired
}

export default TableBody;