import React from 'react';
import PropTypes from "prop-types";


const TextField = (props) => (
    <span>
        <label>{props.label}</label>
        <input type="text" value={props.value} onChange={props.onChange}/>
    </span>
);

TextField.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired
};

export default TextField;