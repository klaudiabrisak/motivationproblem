import React, { Component } from 'react';
import './App.css';
import Table from './Table.js'
import TextField from "./TextField";
import request from 'superagent';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users : [],
            addUser: {name: '', password: ''},
            edit: null,
        };

    }

    async loadUsers() {
        const res = await request.get("/users/find/all");
        this.setState({users: JSON.parse(res.text)});
    }

    async deleteUser(id) {
        const res = await request
            .delete("/secured/deleteUser/" + id);
        console.log(res);
        return JSON.parse(res.statusCode) === 200;
    }

    componentDidMount() {
        this.loadUsers();
    }

    handleRowDelete = async (id) => {
        if (!await this.deleteUser(id)) {
            return;
        }

        const users = this.state.users.slice();
        const index = users.findIndex((item) => item.id === id);
        users.splice(index, 1);
        this.setState({users: users});
    };

    handleRowEdited = (user) => {
        this.setState({edit: user});
    };

    async createUser(user) {
        const res = await request
            .post("/users/create")
            .send(user);
        return {
            success: JSON.parse(res.statusCode) === 200,
            user: JSON.parse(res.text)};
    }


    handleAdd = async () => {
        const result = await this.createUser({...this.state.addUser, username: this.state.addUser.name});
        if (!result.success) {
            return;
        }

        const users = this.state.users.slice();
        users.push(result.user);
        this.setState({
            users: users,
            addUser: {name: '', password: ''}
        });
    };

    handleAddChangeName = (event) => {
        this.setState({addUser: {
            ...this.state.addUser,
            name: event.target.value
            }})
    };

    handleAddChangePassword = (event) => {
        this.setState({addUser: {
                ...this.state.addUser,
                password: event.target.value
            }})
    };

    handleEditChange = (event) => {
        this.setState({edit: {...this.state.edit, name: event.target.value}})
    };

    handleEditDone =  async () => {
        const res = await request
            .put("/users/edit/" + this.state.edit.id)
            .send(this.state.edit);
        console.log(res);
        if (!(JSON.parse(res.statusCode) === 200)) {
            alert("Not changed");
            return;
        }

        const users = this.state.users.slice();
        const index = users.findIndex((item) => item.id === this.state.edit.id);
        users[index] = this.state.edit;
        this.setState({
            users: users,
            edit: null
        });
    };

    render() {
        return (
            <div className="App">
                <Table users={this.state.users} onRowDeleted={this.handleRowDelete} onRowEdited={this.handleRowEdited}/>
                <div>
                    <TextField value={this.state.addUser.name} onChange={this.handleAddChangeName} label={"Name:"}/>
                    <TextField value={this.state.addUser.password} onChange={this.handleAddChangePassword} label={"Password:"}/>
                    <button onClick={this.handleAdd}>Add</button>
                </div>
                {this.state.edit != null &&
                    <div>
                        <TextField value={this.state.edit.name} onChange={this.handleEditChange} label={"Name:"}/>
                        <button onClick={this.handleEditDone}>Save</button>
                    </div>
                }
            </div>
        );
    }
}
export default App;

