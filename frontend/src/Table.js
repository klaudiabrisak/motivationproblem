import React from 'react';
import PropTypes from 'prop-types';
import TableBody from './TableBody'


const Table = props => (
    <table>
        <TableBody users={props.users} onRowDeleted={props.onRowDeleted} onRowEdited={props.onRowEdited}/>
    </table>
);


Table.propTypes = {
    users: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
    })).isRequired,
    onRowDeleted: PropTypes.func.isRequired,
    onRowEdited: PropTypes.func.isRequired
};

export default Table;