import React from "react";
import PropTypes from "prop-types";

const ButtonCell = props =>(
    <td>
        <button onClick={props.onClick}>{props.value}</button>
    </td>
);

ButtonCell.propTypes = {
    value: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default ButtonCell;