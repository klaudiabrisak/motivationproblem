import React from "react";
import PropTypes from "prop-types";


const Cell = props =>(
    <td>{props.value}</td>
);

Cell.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ])
}

export default Cell;